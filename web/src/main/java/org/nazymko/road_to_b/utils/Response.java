package org.nazymko.road_to_b.utils;

import java.util.HashMap;

/**
 * Created by nazymko.patronus@gmail.com
 */
public class Response {
    HashMap data = new HashMap();

    public static Response make() {
        return new Response();
    }

    public Response put(Object key, Object value) {
        data.put(key, value);
        return this;
    }

    public HashMap data() {
        return data;
    }
}
