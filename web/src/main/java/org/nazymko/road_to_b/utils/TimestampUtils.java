package org.nazymko.road_to_b.utils;

import java.sql.Timestamp;
import java.time.Instant;

/**
 * Created by nazymko.patronus@gmail.com
 */
public class TimestampUtils {

    public static Timestamp now() {
        return new Timestamp(System.currentTimeMillis());
    }

    public static Timestamp from(long time) {
        return new Timestamp(time);
    }

    public static Timestamp from(Instant time) {
        return Timestamp.from(time);
    }
}
