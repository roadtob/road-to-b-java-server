package org.nazymko.road_to_b.api.v1;

import org.nazymko.dao.SitsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;


/**
 * Created by nazymko.patronus@gmail.com.
 */
@Controller
@RequestMapping({"/api/v1/sits", "/api/latest/sits"})
public class SitsController {
    @Autowired
    SitsDao sitsDao;

    @RequestMapping(method = RequestMethod.POST)
    public Object register(Model model) {
        throw new NotImplementedException();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    @ResponseBody
    public List registerGet(@PathVariable Integer id) {
        return sitsDao.fetchById(id);
    }


}
