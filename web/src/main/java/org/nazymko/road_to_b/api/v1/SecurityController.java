package org.nazymko.road_to_b.api.v1;

import org.nazymko.road_to_b.dao.tables.pojos.Drivers;
import org.nazymko.road_to_b.services.driver.LoginService;
import org.nazymko.road_to_b.services.driver.RegistrationResponse;
import org.nazymko.road_to_b.services.driver.RegistrationService;
import org.nazymko.road_to_b.services.driver.data.DriverLoginResponse;
import org.nazymko.road_to_b.services.driver.data.GeneralLoginRequest;
import org.nazymko.road_to_b.services.driver.data.GeneralRegistrationRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

/**
 * Created by nazymko.patronus@gmail.com
 */
@Controller
@RequestMapping({"/api/v1/security", "/api/latest/security"})
public class SecurityController {
    @Resource
    RegistrationService registrationService;

    @Resource
    LoginService loginService;

    @ResponseBody
    @RequestMapping(value = "login", method = RequestMethod.POST)
    DriverLoginResponse login(@RequestBody GeneralLoginRequest userData) {
        return loginService.doLogin(userData.getLogin(), userData.getPassword());
    }

    @RequestMapping(value = "register", method = RequestMethod.POST)
    @ResponseBody
    RegistrationResponse register(@RequestBody GeneralRegistrationRequest userData) throws NoSuchAlgorithmException {
        Optional<Drivers> driver = registrationService.register(userData);

        if (driver.isPresent()) {
            return new RegistrationResponse(true, "Success", driver.get());
        } else {
            return new RegistrationResponse(false, "Failed", null);
        }

    }
}
