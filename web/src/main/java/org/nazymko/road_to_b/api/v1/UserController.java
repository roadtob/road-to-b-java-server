package org.nazymko.road_to_b.api.v1;

import lombok.extern.log4j.Log4j2;
import org.nazymko.dao.*;
import org.nazymko.road_to_b.common.sec.user.UserParams;
import org.nazymko.road_to_b.dao.tables.pojos.Orders;
import org.nazymko.road_to_b.dao.tables.pojos.UserContacts;
import org.nazymko.road_to_b.dao.tables.pojos.UserIdentities;
import org.nazymko.road_to_b.dao.tables.pojos.Users;
import org.nazymko.road_to_b.services.user.UserRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by nazymko.patronus@gmail.com.
 */
@Log4j2
@Controller
@RequestMapping({"/api/v1/users", "/api/latest/users"})
public class UserController {
    @Autowired
    UserDao tripDao;
    @Autowired
    OrderDao orderDao;
    @Autowired
    UserIdentifiersDao userIdentitiesDao;
    @Autowired
    UserContactDao userContactDao;
    @Resource
    UserRegistrationService userService;
    @Resource
    UserCredentialsDao userCredentialsDao;

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    @ResponseBody
    public Users registerGet(@PathVariable Integer id) {
        return tripDao.fetchOneById(id);
    }

    @RequestMapping(value = "{id}/orders", method = RequestMethod.GET)
    @ResponseBody
    public List<Orders> userOrders(@PathVariable Integer id) {
        return orderDao.fetchByUserId(id);
    }

    @RequestMapping(value = "{id}/identifiers", method = RequestMethod.GET)
    @ResponseBody
    public List<UserIdentities> userIdentifiers(@PathVariable Integer id) {
        return userIdentitiesDao.fetchByUserId(id);
    }

    /**
     * Return user contacts
     */
    @RequestMapping(value = "{id}/contacts", method = RequestMethod.GET)
    @ResponseBody
    public HashMap<String, String> userContacts(@PathVariable Integer id) {
        List<UserContacts> userContactses = userContactDao.fetchByUserId(id);
        HashMap<String, String> res = new HashMap<>();
        for (UserContacts contacts : userContactses) {
            res.put(contacts.getProp(), contacts.getValue());
        }
        return res;
    }

    /**
     * User login
     */
    @ResponseBody
    @RequestMapping(value = "login", method = RequestMethod.POST)
    public Map<String, Object> login(@RequestBody UserParams userInfo) {
        return userService.login(userInfo);
    }


    /**
     * User registration
     */
    @ResponseBody
    @RequestMapping(value = "register", method = RequestMethod.POST)
    public Users register(@RequestBody UserParams userInfo) {
        return userService.register(userInfo);
    }

    @ExceptionHandler
    void onError(Exception any) {
        log.error("Exception", any);
    }
}
