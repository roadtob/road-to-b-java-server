package org.nazymko.road_to_b.api.v1;

import org.nazymko.dao.PhoneCodesDao;
import org.nazymko.road_to_b.dao.tables.pojos.PhoneCodes;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by nazymko.patronus@gmail.com
 */

@RestController
@RequestMapping({"/api/v1/countries", "/api/latest/countries"})
public class CountryController {
    @Resource
    PhoneCodesDao phoneCodesDao;

    @RequestMapping("codes")
    public List<PhoneCodes> codes() {
        return phoneCodesDao.findAll();
    }

}
