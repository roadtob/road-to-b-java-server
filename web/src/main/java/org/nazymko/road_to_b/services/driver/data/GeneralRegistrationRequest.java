package org.nazymko.road_to_b.services.driver.data;

import lombok.Data;

/**
 * Created by nazymko.patronus@gmail.com
 */
@Data
public class GeneralRegistrationRequest {
    String login;
    String password;
    String firstName;
    String lastName;
}
