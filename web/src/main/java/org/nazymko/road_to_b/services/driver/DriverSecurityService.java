package org.nazymko.road_to_b.services.driver;

import lombok.extern.log4j.Log4j2;
import org.nazymko.dao.DriverCredentialsDao;
import org.nazymko.dao.DriverDao;
import org.nazymko.dao.DriverTypesDao;
import org.nazymko.dao.UserCredentialsDao;
import org.nazymko.road_to_b.dao.tables.pojos.DriverCredentials;
import org.nazymko.road_to_b.dao.tables.pojos.Drivers;
import org.nazymko.road_to_b.dao.tables.records.DriverCredentialsRecord;
import org.nazymko.road_to_b.dao.tables.records.UserCredentialsRecord;
import org.nazymko.road_to_b.services.driver.data.DriverLoginResponse;
import org.nazymko.road_to_b.services.driver.data.GeneralRegistrationRequest;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.nazymko.road_to_b.dao.tables.DriverCredentials.DRIVER_CREDENTIALS;
import static org.nazymko.road_to_b.dao.tables.Drivers.DRIVERS;

/**
 * Created by nazymko.patronus@gmail.com
 */

@Log4j2
@Service("securityService")
public class DriverSecurityService {

    @Resource
    DriverCredentialsDao credentialsDao;

    @Resource
    DriverDao driverDao;

    @Resource
    DriverTypesDao types;

    @Resource
    UserCredentialsDao userCredentialsDao;

    @Resource
    DriverCredentialsDao driverCredentialsDao;

    private void savePassword(Integer driverId, String password) throws NoSuchAlgorithmException {

        byte[] salt = getSalt();
        String securePassword = getSecurePassword(password, salt);

        DriverCredentials credentials = new DriverCredentials()
                .setDriverId(driverId)
                .setHash(securePassword)
                .setSalt(salt);

        credentialsDao.insert(credentials);

    }

    public Optional<Drivers> register(GeneralRegistrationRequest userData) throws NoSuchAlgorithmException {
        List<Drivers> drivers = driverDao.fetchByLogin(userData.getLogin());
        if (!drivers.isEmpty()) {
//        log "Avoiding login duplicating. Terminating this process");
            return Optional.empty();
        }

        Drivers driver = new Drivers();
        driver.setLogin(userData.getLogin());
        driver.setFirstName(userData.getFirstName());
        driver.setLastName(userData.getLastName());
        driver.setDriverType(types.regular());

        driverDao.insert(driver);

        Drivers newDriver = driverDao.fetchByLogin(userData.getLogin()).get(0);

        savePassword(newDriver.getId(), userData.getPassword());
        return Optional.of(newDriver);

    }


    public Optional<Drivers> find(String login, String proposedPassword) {
        Drivers driver = driverDao.fetchOne(DRIVERS.LOGIN, login);
        if (driver == null) {
            return Optional.empty();
        }

        DriverCredentials credentials = credentialsDao.fetchOne(DRIVER_CREDENTIALS.DRIVER_ID, driver.getId());

        byte[] salt = credentials.getSalt();
        String securePassword = getSecurePassword(proposedPassword, salt);

        if (credentials.getHash().equals(securePassword)) {
            return Optional.of(driver);
        } else {
            return Optional.empty();
        }
    }

    public boolean isRegistered(String login) {
        return !driverDao.fetchByLogin(login).isEmpty();
    }


    private byte[] getSalt() throws NoSuchAlgorithmException {
        //Always use a SecureRandom generator
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        //Create array for salt
        byte[] salt = new byte[512];
        //Get a random salt
        sr.nextBytes(salt);
        //return salt
        return salt;
    }


    private String getSecurePassword(String passwordToHash, byte[] salt) {
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for SHA-512
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            //Add password bytes to digest
            md.update(salt);
            //Get the hash's bytes
            byte[] bytes = md.digest(passwordToHash.getBytes());
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    DriverLoginResponse login(String login, String driverPassword) {
        Optional<Drivers> driver = find(login, driverPassword);
        if (driver.isPresent()) {
            String newToken = UUID.randomUUID().toString();
            credentialsDao.updateToken(newToken, driver.get().getId());
            return new DriverLoginResponse(true, newToken, driver.get());
        } else {
            return new DriverLoginResponse(false, null, null);

        }
    }


    public boolean hasAcces(Integer userId, String accessToken) {

        Optional<UserCredentialsRecord> user = userCredentialsDao.findByUserId(userId);
        if (user.isPresent() && user.get().getAccesToken() != null) {
            return user.get().getAccesToken().equals(accessToken);
        } else {
            return false;
        }
    }


    public boolean verifyDriverAccess(Integer driverId, String accessToken) {

        Optional<DriverCredentialsRecord> driverCredentialsRecord = driverCredentialsDao.finByDriverId(driverId);
        if (driverCredentialsRecord.isPresent() && driverCredentialsRecord.get().getAccesToken() != null) {
            return driverCredentialsRecord.get().getAccesToken().equals(accessToken);
        }

        return true;
    }


}
