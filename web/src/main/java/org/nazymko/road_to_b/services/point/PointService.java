package org.nazymko.road_to_b.services.point;

import org.jooq.Record1;
import org.jooq.Result;
import org.nazymko.dao.PointDao;
import org.nazymko.dao.RoutPointMappingsDao;
import org.nazymko.dao.TripDao;
import org.nazymko.road_to_b.dao.tables.pojos.Points;
import org.nazymko.road_to_b.dao.tables.pojos.Trips;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static org.nazymko.road_to_b.dao.Tables.ROUT_POINT_MAPPINGS;
import static org.nazymko.road_to_b.utils.ListUtil.toArray;

/**
 * Created by nazymko.patronus@gmail.com
 */
@Service
public class PointService {
    private static final Integer START = 0;
    @Resource
    PointDao pointDao;
    @Resource
    TripDao tripDao;
    @Resource
    RoutPointMappingsDao routPointMappingsDao;


    public List<Trips> tripsFromPoint(Long id) {
        Result<Record1<Integer>> fetchResult = routPointMappingsDao.getDsl()
                .select(ROUT_POINT_MAPPINGS.ID).from(ROUT_POINT_MAPPINGS)
                .where(ROUT_POINT_MAPPINGS.POINT_ID.eq(id))
                .and(ROUT_POINT_MAPPINGS.ORDERING.eq(START)).fetch();


        List<Integer> ids = new ArrayList<>();
        for (Record1<Integer> fetchRecord : fetchResult) {
            ids.add(fetchRecord.getValue(ROUT_POINT_MAPPINGS.ID));
        }

        return tripDao.fetchById(toArray(ids));

    }

    public List<Points> all() {
        return pointDao.findAll();
    }

}
