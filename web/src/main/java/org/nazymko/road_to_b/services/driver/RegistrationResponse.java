package org.nazymko.road_to_b.services.driver;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.nazymko.road_to_b.dao.tables.pojos.Drivers;

/**
 * Created by nazymko.patronus@gmail.com
 */
@Data
@AllArgsConstructor
public class RegistrationResponse {
    boolean registered;
    String message;
    Drivers driver;
}
