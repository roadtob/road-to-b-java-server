package org.nazymko.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * Created by nazymko.patronus@gmail.com.
 */
public class PointDao extends org.nazymko.road_to_b.dao.tables.daos.PointsDao implements ContextSupply {
    @Resource
    DataSource dataSource;

    @Override
    public DataSource getDatasource() {
        return dataSource;
    }

    boolean exist(Long id) {
        return findById(id) != null;
    }
}
