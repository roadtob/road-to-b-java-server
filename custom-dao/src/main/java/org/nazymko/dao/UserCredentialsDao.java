package org.nazymko.dao;

import org.nazymko.road_to_b.dao.tables.records.UserCredentialsRecord;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Optional;

import static org.nazymko.road_to_b.dao.Tables.USER_CREDENTIALS;

/**
 * Created by nazymko.patronus@gmail.com.
 */
public class UserCredentialsDao extends org.nazymko.road_to_b.dao.tables.daos.UserCredentialsDao implements ContextSupply {
    @Resource
    DataSource dataSource;

    @Override
    public DataSource getDatasource() {
        return dataSource;
    }


    public Optional<UserCredentialsRecord> findByUserId(Integer userId) {
        UserCredentialsRecord userCredentialsRecord = getDsl().selectFrom(USER_CREDENTIALS).where(USER_CREDENTIALS.USER_ID.eq(userId)).fetchOne();

        return Optional.ofNullable(userCredentialsRecord);
    }
}
