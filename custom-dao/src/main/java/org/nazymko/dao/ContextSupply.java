package org.nazymko.dao;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import javax.sql.DataSource;

/**
 * Created by nazymko.patronus@gmail.com
 */
public interface ContextSupply {
    default DSLContext getDsl() {
        return DSL.using(getDatasource(), SQLDialect.MYSQL);
    }

    DataSource getDatasource();
}
