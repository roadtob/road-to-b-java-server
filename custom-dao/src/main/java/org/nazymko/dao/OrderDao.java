package org.nazymko.dao;

import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.nazymko.road_to_b.dao.tables.pojos.Orders;
import org.nazymko.road_to_b.dao.tables.records.OrdersRecord;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * Created by nazymko.patronus@gmail.com.
 */
public class OrderDao extends org.nazymko.road_to_b.dao.tables.daos.OrdersDao implements ContextSupply {
    @Resource
    DataSource dataSource;

    @Override
    public DataSource getDatasource() {
        return dataSource;
    }
    /** Store and fill object with database values */
    public void store(Orders order) {
        OrdersRecord _record = new OrdersRecord();
        _record.setIsDisabled(order.getIsDisabled());
        _record.setSitId(order.getSitId());
        _record.setTripId(order.getTripId());
        _record.setUserId(order.getUserId());
        _record.setIsDisabled(false);

        store(_record);

        order.setId(_record.getId());
        order.setChangedAt(_record.getChangedAt());
        order.setCreatedAt(_record.getCreatedAt());
        order.setIsDisabled(false);
    }

    public void store(OrdersRecord record) {
        DSLContext dslContext = DSL();
        dslContext.attach(record);
        record.store();
        dslContext.close();
    }

    private DSLContext DSL() {
        return DSL.using(configuration());
    }


}
