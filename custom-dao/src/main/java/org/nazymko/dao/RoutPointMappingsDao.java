package org.nazymko.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * Created by nazymko.patronus@gmail.com.
 */
public class RoutPointMappingsDao extends org.nazymko.road_to_b.dao.tables.daos.RoutPointMappingsDao implements ContextSupply {
    @Resource
    DataSource dataSource;

    @Override
    public DataSource getDatasource() {
        return dataSource;
    }
}
