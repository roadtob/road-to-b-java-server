SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE driver_credentials
(
  id            INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  driver_id     INT(11)             NOT NULL,
  hash          VARCHAR(512)        NOT NULL,
  salt          BLOB                NOT NULL,
  acces_token   VARCHAR(36),
  time_to_leave TIMESTAMP
);
CREATE TABLE driver_props
(
  id          INT(11) PRIMARY KEY                   NOT NULL AUTO_INCREMENT,
  driver_id   INT(11)                               NOT NULL,
  prop        VARCHAR(1024)                         NOT NULL,
  value       VARCHAR(1024)                         NOT NULL,
  is_disabled TINYINT(4)                            NOT NULL,
  changed_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  created_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL
);
CREATE TABLE driver_types
(
  id          INT(11) PRIMARY KEY                   NOT NULL AUTO_INCREMENT,
  description VARCHAR(1024)                         NOT NULL,
  name        VARCHAR(1024)                         NOT NULL,
  is_disabled TINYINT(4)                            NOT NULL,
  changed_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  created_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL
);
CREATE TABLE drivers
(
  id          INT(11) PRIMARY KEY                   NOT NULL AUTO_INCREMENT,
  first_name  VARCHAR(1024)                         NOT NULL,
  last_name   VARCHAR(1024)                         NOT NULL,
  is_disabled TINYINT(4) DEFAULT '0'                NOT NULL,
  driver_type INT(11)                               NOT NULL,
  changed_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  created_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  login       VARCHAR(128)                          NOT NULL
);
CREATE TABLE orders
(
  id          INT(11) PRIMARY KEY                   NOT NULL AUTO_INCREMENT,
  sit_id      INT(11)                               NOT NULL,
  trip_id     INT(11)                               NOT NULL,
  user_id     INT(11)                               NOT NULL,
  is_disabled TINYINT(4)                            NOT NULL,
  changed_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  created_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL
);
CREATE TABLE orders_status_mappings
(
  id         INT(11) PRIMARY KEY                   NOT NULL AUTO_INCREMENT,
  name       VARCHAR(128)                          NOT NULL,
  changed_at TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  created_at TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL
);
CREATE TABLE orders_statuses
(
  id         INT(11) PRIMARY KEY                   NOT NULL AUTO_INCREMENT,
  order_id   INT(11)                               NOT NULL,
  driver_id  INT(11)                               NOT NULL,
  status_id  INT(11)                               NOT NULL,
  message    LONGTEXT                              NOT NULL,
  created_at TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  changed_at TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL
);
CREATE TABLE phone_codes
(
  id                  INT(11) PRIMARY KEY      NOT NULL AUTO_INCREMENT,
  country_name_native VARCHAR(512)             NOT NULL,
  country_name        VARCHAR(512)             NOT NULL,
  country_code        VARCHAR(32)              NOT NULL,
  country_call_code   VARCHAR(32) DEFAULT 'NA' NOT NULL
);
CREATE TABLE points
(
  id          BIGINT(20) PRIMARY KEY                NOT NULL AUTO_INCREMENT,
  name        LONGTEXT                              NOT NULL,
  longitude   DOUBLE                                NOT NULL,
  latitude    DOUBLE                                NOT NULL,
  is_disabled TINYINT(4)                            NOT NULL,
  changed_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  created_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL
);
CREATE TABLE rout_point_mappings
(
  routine_id  INT(11)                               NOT NULL,
  point_id    BIGINT(20),
  ordering    INT(11)                               NOT NULL,
  is_disabled TINYINT(4)                            NOT NULL,
  changed_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  created_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  id          INT(11) PRIMARY KEY                   NOT NULL AUTO_INCREMENT
);
CREATE TABLE routines
(
  id          INT(11) PRIMARY KEY                   NOT NULL AUTO_INCREMENT,
  description VARCHAR(1024)                         NOT NULL,
  is_disabled TINYINT(4)                            NOT NULL,
  changed_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  created_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL
);
CREATE TABLE sits
(
  id           INT(11) PRIMARY KEY                   NOT NULL AUTO_INCREMENT,
  transport_id INT(11)                               NOT NULL,
  trip_id      INT(11)                               NOT NULL,
  place_number SMALLINT(6)                           NOT NULL,
  is_disabled  TINYINT(4)                            NOT NULL,
  changed_at   TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  created_at   TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL
);
CREATE TABLE transport_props
(
  id           INT(11) PRIMARY KEY                   NOT NULL AUTO_INCREMENT,
  transport_id INT(11)                               NOT NULL,
  prop         VARCHAR(1024)                         NOT NULL,
  value        VARCHAR(1024)                         NOT NULL,
  is_disabled  TINYINT(4) DEFAULT '0'                NOT NULL,
  changed_at   TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  created_at   TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL
);
CREATE TABLE transports
(
  id          INT(11) PRIMARY KEY                   NOT NULL,
  name        VARCHAR(1024)                         NOT NULL,
  is_disabled TINYINT(4)                            NOT NULL,
  changed_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  created_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL
);
CREATE TABLE trips
(
  id              INT(11) PRIMARY KEY                   NOT NULL AUTO_INCREMENT,
  transport_id    INT(11)                               NOT NULL,
  driver_id       INT(11)                               NOT NULL,
  departure_date  DATETIME                              NOT NULL,
  arrival_date    TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  is_disabled     TINYINT(4)                            NOT NULL,
  routine_id      INT(11)                               NOT NULL,
  changed_at      TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  created_at      TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  price           DOUBLE DEFAULT '0'                    NOT NULL,
  price_currency  VARCHAR(16)                                    DEFAULT 'RYB',
  departure_point BIGINT(20)                            NOT NULL,
  arrival_point   BIGINT(20)
);
CREATE TABLE user_contacts
(
  id          INT(11) PRIMARY KEY                   NOT NULL AUTO_INCREMENT,
  user_id     INT(11)                               NOT NULL,
  prop        VARCHAR(128)                          NOT NULL,
  value       VARCHAR(1024)                         NOT NULL,
  is_disabled TINYINT(4),
  changed_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  created_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL
);
CREATE TABLE user_identities
(
  id         INT(11) PRIMARY KEY                   NOT NULL AUTO_INCREMENT,
  user_id    INT(11)                               NOT NULL,
  phone      VARCHAR(32)                           NOT NULL,
  changed_at TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  created_at TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL
);
CREATE TABLE user_roles
(
  id         INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  type       VARCHAR(128),
  notes      VARCHAR(1024),
  is_default TINYINT(4)                   DEFAULT '0',
  created_at TIMESTAMP                    DEFAULT 'CURRENT_TIMESTAMP'
);
CREATE TABLE users
(
  id          INT(11) PRIMARY KEY                   NOT NULL AUTO_INCREMENT,
  name        VARCHAR(1024)                         NOT NULL,
  is_disabled TINYINT(4)                                     DEFAULT '0',
  changed_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  created_at  TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
  uuid        VARCHAR(36)                           NOT NULL
);
CREATE TABLE users_roles_mapping
(
  id      INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  user_id INT(11),
  role_id INT(11)
);
ALTER TABLE driver_credentials
  ADD FOREIGN KEY (driver_id) REFERENCES drivers (id);
CREATE INDEX driver_identity_driver_id___fk ON driver_credentials (driver_id);
CREATE UNIQUE INDEX driver_identity_id_uindex ON driver_credentials (id);
ALTER TABLE driver_props
  ADD FOREIGN KEY (driver_id) REFERENCES drivers (id);
CREATE INDEX driver_id ON driver_props (driver_id);
CREATE UNIQUE INDEX unique_id ON driver_types (id);
ALTER TABLE drivers
  ADD FOREIGN KEY (driver_type) REFERENCES driver_types (id);
CREATE INDEX driver_type ON drivers (driver_type);
ALTER TABLE orders
  ADD FOREIGN KEY (sit_id) REFERENCES sits (id);
ALTER TABLE orders
  ADD FOREIGN KEY (trip_id) REFERENCES trips (id);
ALTER TABLE orders
  ADD FOREIGN KEY (user_id) REFERENCES users (id);
CREATE INDEX sit_id ON orders (sit_id);
CREATE INDEX trip_id ON orders (trip_id);
CREATE UNIQUE INDEX unique_id ON orders (id);
CREATE INDEX user_id ON orders (user_id);
CREATE UNIQUE INDEX unique_name ON orders_status_mappings (name);
ALTER TABLE orders_statuses
  ADD FOREIGN KEY (order_id) REFERENCES orders (id);
ALTER TABLE orders_statuses
  ADD FOREIGN KEY (driver_id) REFERENCES drivers (id);
ALTER TABLE orders_statuses
  ADD FOREIGN KEY (status_id) REFERENCES orders_status_mappings (id);
CREATE INDEX driver_id ON orders_statuses (driver_id);
CREATE INDEX order_id ON orders_statuses (order_id);
CREATE INDEX status ON orders_statuses (status_id);
CREATE UNIQUE INDEX phone_codes_country_code_uindex ON phone_codes (country_code);
CREATE UNIQUE INDEX phone_codes_country_name_native_uindex ON phone_codes (country_name_native);
CREATE UNIQUE INDEX phone_codes_country_name_uindex ON phone_codes (country_name);
CREATE UNIQUE INDEX phone_codes_id_uindex ON phone_codes (id);
ALTER TABLE rout_point_mappings
  ADD FOREIGN KEY (routine_id) REFERENCES routines (id);
ALTER TABLE rout_point_mappings
  ADD FOREIGN KEY (point_id) REFERENCES points (id);
CREATE INDEX point_id ON rout_point_mappings (point_id);
CREATE INDEX routine_id ON rout_point_mappings (routine_id);
CREATE UNIQUE INDEX rout_point_mappings_id_uindex ON rout_point_mappings (id);
CREATE UNIQUE INDEX unique_id ON routines (id);
ALTER TABLE sits
  ADD FOREIGN KEY (transport_id) REFERENCES transports (id);
ALTER TABLE sits
  ADD FOREIGN KEY (trip_id) REFERENCES trips (id);
CREATE INDEX transport_id ON sits (transport_id);
CREATE INDEX trip_id ON sits (trip_id);
ALTER TABLE transport_props
  ADD FOREIGN KEY (transport_id) REFERENCES transports (id);
CREATE INDEX transport_id ON transport_props (transport_id);
ALTER TABLE trips
  ADD FOREIGN KEY (arrival_point) REFERENCES points (id);
ALTER TABLE trips
  ADD FOREIGN KEY (departure_point) REFERENCES points (id);
ALTER TABLE trips
  ADD FOREIGN KEY (transport_id) REFERENCES transports (id);
ALTER TABLE trips
  ADD FOREIGN KEY (driver_id) REFERENCES drivers (id);
ALTER TABLE trips
  ADD FOREIGN KEY (routine_id) REFERENCES routines (id);
CREATE INDEX driver_id ON trips (driver_id);
CREATE INDEX routine_id ON trips (routine_id);
CREATE INDEX transport_id ON trips (transport_id);
CREATE INDEX trips_arrival_points__fk ON trips (arrival_point);
CREATE INDEX trips_departure_points__fk ON trips (departure_point);
ALTER TABLE user_contacts
  ADD FOREIGN KEY (user_id) REFERENCES users (id);
CREATE UNIQUE INDEX unique_id ON user_contacts (id);
CREATE INDEX user_id ON user_contacts (user_id);
ALTER TABLE user_identities
  ADD FOREIGN KEY (user_id) REFERENCES users (id);
CREATE INDEX user_id ON user_identities (user_id);
CREATE UNIQUE INDEX user_roles_id_uindex ON user_roles (id);
CREATE UNIQUE INDEX unique_id ON users (id);
ALTER TABLE users_roles_mapping
  ADD FOREIGN KEY (user_id) REFERENCES users (id);
ALTER TABLE users_roles_mapping
  ADD FOREIGN KEY (role_id) REFERENCES user_roles (id);
CREATE UNIQUE INDEX users_roles_mapping_id_uindex ON users_roles_mapping (id);
CREATE INDEX users_roles_mapping_users__fk ON users_roles_mapping (user_id);
CREATE INDEX users_roles_mapping_user_roles_id_fk ON users_roles_mapping (role_id);

SET FOREIGN_KEY_CHECKS = 1;