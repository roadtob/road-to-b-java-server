# tables renaming
ALTER TABLE trip RENAME TO trips;
ALTER TABLE point RENAME TO points;
ALTER TABLE driver RENAME TO drivers;
ALTER TABLE user_contact RENAME TO user_contacts;
ALTER TABLE user_identity RENAME TO user_identities;
ALTER TABLE user RENAME TO users;
ALTER TABLE transport RENAME TO transports;
ALTER TABLE routine RENAME TO routines;
ALTER TABLE rout_point_mapping RENAME TO rout_point_mappings;
ALTER TABLE orders_status_mapping RENAME TO orders_status_mappings;
ALTER TABLE orders_status RENAME TO orders_statuses;