INSERT INTO road_to_b_dev.users (id, name, is_disabled, changed_at, created_at, uuid)
VALUES (1, 'Вася', 0, '2016-05-19 00:57:15', '2016-05-19 00:57:16', '11111');

INSERT INTO road_to_b_dev.user_identities (user_id, phone, changed_at, created_at)
VALUES (1, '+385412245', '2016-05-19 00:59:02', '2016-05-19 00:59:03');

INSERT INTO road_to_b_dev.user_credentials (user_id, acces_token, salt, password, last_change)
VALUES (1, '1234567890', 0x31, '1', '2016-05-19 01:00:06');

INSERT INTO road_to_b_dev.user_contacts (user_id, prop, value, is_disabled, changed_at, created_at)
VALUES (1, 'phone', '+625541414', 0, '2016-05-19 01:00:50', '2016-05-19 01:00:52');

INSERT INTO road_to_b_dev.drivers (id, first_name, last_name, is_disabled, driver_type, changed_at, created_at, login)
VALUES (1, 'Миша', 'Петренко', 0, (SELECT id
                                   FROM driver_types
                                   WHERE name = 'regular'), '2016-05-19 01:03:20', '2016-05-19 01:03:22', '+365521451');


INSERT INTO road_to_b_dev.drivers (id, first_name, last_name, is_disabled, driver_type, changed_at, created_at, login)
VALUES (2, 'Антон', 'Мошенник', 1, (SELECT id
                                    FROM driver_types
                                    WHERE name = 'regular'), '2016-05-19 01:03:20', '2016-05-19 01:03:22',
        '+641515131');


INSERT INTO road_to_b_dev.points (id, name, longitude, latitude, is_disabled, changed_at, created_at)
VALUES (1, 'Бобруйск', 1, 1, 0, '2016-05-19 01:07:27', '2016-05-19 01:07:29');

INSERT INTO road_to_b_dev.points (id, name, longitude, latitude, is_disabled, changed_at, created_at)
VALUES (2, 'Мухино', 2, 2, 0, '2016-05-19 01:07:44', '2016-05-19 01:07:45');

INSERT INTO road_to_b_dev.routines (id, description, is_disabled, changed_at, created_at)
VALUES (1, 'Бобруйск-Мухино', 0, '2016-05-19 01:10:03', '2016-05-19 01:10:05');

INSERT INTO road_to_b_dev.rout_point_mappings (routine_id, point_id, ordering, is_disabled, changed_at)
VALUES (1, 1, 0, 0, '2016-05-19 01:12:08');

INSERT INTO road_to_b_dev.rout_point_mappings (routine_id, point_id, ordering, is_disabled, changed_at)
VALUES (1, 2, 1, 0, '2016-05-19 01:13:02');

INSERT INTO road_to_b_dev.transports (name, is_disabled, changed_at, created_at, uuid)
VALUES ('Audi', 0, '2016-05-19 01:23:21', '2016-05-19 01:23:22', '775a8372-1d47-11e6-b6ba-3e1d05defe78');

INSERT INTO road_to_b_dev.trips (transport_id, driver_id,
                                 departure_date, arrival_date,
                                 is_disabled, routine_id,
                                 changed_at, created_at, price,
                                 price_currency, departure_point, arrival_point)
VALUES
  ((SELECT id
    FROM transports
    WHERE uuid = '775a8372-1d47-11e6-b6ba-3e1d05defe78'/*Audi*/), 1, '2016-05-19 01:27:10', '2016-05-30 06:27:11', 0, 1,
                                                                  '2016-05-19 01:27:25', '2016-05-19 01:27:27', 51000,
                                                                  'RYB',
                                                                  1, 2);