ALTER TABLE road_to_b_dev.trips
  ADD sits_free SMALLINT DEFAULT 0 NULL;
ALTER TABLE road_to_b_dev.trips
  ADD sits_total SMALLINT DEFAULT 0 NULL;

ALTER TABLE road_to_b_dev.sits
  ADD user_id INT NULL;

ALTER TABLE road_to_b_dev.sits
  ADD CONSTRAINT sits_users__fk
FOREIGN KEY (user_id) REFERENCES users (id)
  ON DELETE SET NULL;