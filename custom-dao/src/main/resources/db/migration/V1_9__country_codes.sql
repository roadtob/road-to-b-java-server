# https://countrycode.org/belarus

CREATE TABLE phone_codes
(
  id                  INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  country_name_native VARCHAR(512)        NOT NULL,
  country_name        VARCHAR(512)        NOT NULL,
  country_code        VARCHAR(32)         NOT NULL,
  country_call_code   VARCHAR(32)         NOT NULL
);
CREATE UNIQUE INDEX phone_codes_country_code_uindex ON phone_codes (country_code);
CREATE UNIQUE INDEX phone_codes_country_name_native_uindex ON phone_codes (country_name_native);
CREATE UNIQUE INDEX phone_codes_country_name_uindex ON phone_codes (country_name);
CREATE UNIQUE INDEX phone_codes_id_uindex ON phone_codes (id);

INSERT INTO road_to_b_dev.phone_codes (country_name_native, country_name, country_code, country_call_code)
VALUES ('Україна', 'Ukraine', 'UA', '+380');
INSERT INTO road_to_b_dev.phone_codes (country_name_native, country_name, country_code, country_call_code)
VALUES ('Беларусь', 'Belarus', 'BY', '+375');

INSERT INTO road_to_b_dev.phone_codes (country_name_native, country_name, country_code, country_call_code)
VALUES ('Россия', 'Russia', 'RU', '+7');