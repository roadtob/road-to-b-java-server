SET FOREIGN_KEY_CHECKS = 0;
ALTER TABLE road_to_b_dev.rout_point_mappings
  ADD id INT NOT NULL PRIMARY KEY AUTO_INCREMENT;
CREATE UNIQUE INDEX rout_point_mappings_id_uindex ON road_to_b_dev.rout_point_mappings (id);
SET FOREIGN_KEY_CHECKS =1;
