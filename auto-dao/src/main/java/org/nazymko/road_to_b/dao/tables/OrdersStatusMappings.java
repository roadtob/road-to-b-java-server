/**
 * This class is generated by jOOQ
 */
package org.nazymko.road_to_b.dao.tables;


import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Identity;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;
import org.nazymko.road_to_b.dao.Keys;
import org.nazymko.road_to_b.dao.RoadToBDev;
import org.nazymko.road_to_b.dao.tables.records.OrdersStatusMappingsRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.2"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class OrdersStatusMappings extends TableImpl<OrdersStatusMappingsRecord> {

	private static final long serialVersionUID = -288727588;

	/**
	 * The reference instance of <code>road_to_b_dev.orders_status_mappings</code>
	 */
	public static final OrdersStatusMappings ORDERS_STATUS_MAPPINGS = new OrdersStatusMappings();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<OrdersStatusMappingsRecord> getRecordType() {
		return OrdersStatusMappingsRecord.class;
	}

	/**
	 * The column <code>road_to_b_dev.orders_status_mappings.id</code>.
	 */
	public final TableField<OrdersStatusMappingsRecord, Integer> ID = createField("id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

	/**
	 * The column <code>road_to_b_dev.orders_status_mappings.name</code>.
	 */
	public final TableField<OrdersStatusMappingsRecord, String> NAME = createField("name", org.jooq.impl.SQLDataType.VARCHAR.length(128).nullable(false), this, "");

	/**
	 * The column <code>road_to_b_dev.orders_status_mappings.changed_at</code>.
	 */
	public final TableField<OrdersStatusMappingsRecord, Timestamp> CHANGED_AT = createField("changed_at", org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>road_to_b_dev.orders_status_mappings.created_at</code>.
	 */
	public final TableField<OrdersStatusMappingsRecord, Timestamp> CREATED_AT = createField("created_at", org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false).defaulted(true), this, "");

	/**
	 * Create a <code>road_to_b_dev.orders_status_mappings</code> table reference
	 */
	public OrdersStatusMappings() {
		this("orders_status_mappings", null);
	}

	/**
	 * Create an aliased <code>road_to_b_dev.orders_status_mappings</code> table reference
	 */
	public OrdersStatusMappings(String alias) {
		this(alias, ORDERS_STATUS_MAPPINGS);
	}

	private OrdersStatusMappings(String alias, Table<OrdersStatusMappingsRecord> aliased) {
		this(alias, aliased, null);
	}

	private OrdersStatusMappings(String alias, Table<OrdersStatusMappingsRecord> aliased, Field<?>[] parameters) {
		super(alias, RoadToBDev.ROAD_TO_B_DEV, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Identity<OrdersStatusMappingsRecord, Integer> getIdentity() {
		return Keys.IDENTITY_ORDERS_STATUS_MAPPINGS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<OrdersStatusMappingsRecord> getPrimaryKey() {
		return Keys.KEY_ORDERS_STATUS_MAPPINGS_PRIMARY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<OrdersStatusMappingsRecord>> getKeys() {
		return Arrays.<UniqueKey<OrdersStatusMappingsRecord>>asList(Keys.KEY_ORDERS_STATUS_MAPPINGS_PRIMARY, Keys.KEY_ORDERS_STATUS_MAPPINGS_UNIQUE_NAME);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OrdersStatusMappings as(String alias) {
		return new OrdersStatusMappings(alias, this);
	}

	/**
	 * Rename this table
	 */
	public OrdersStatusMappings rename(String name) {
		return new OrdersStatusMappings(name, null);
	}
}
