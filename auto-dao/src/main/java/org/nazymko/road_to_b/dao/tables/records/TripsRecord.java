/**
 * This class is generated by jOOQ
 */
package org.nazymko.road_to_b.dao.tables.records;


import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record15;
import org.jooq.Row15;
import org.jooq.impl.UpdatableRecordImpl;
import org.nazymko.road_to_b.dao.tables.Trips;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.2"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
@Entity
@Table(name = "trips", schema = "road_to_b_dev")
public class TripsRecord extends UpdatableRecordImpl<TripsRecord> implements Record15<Integer, Integer, Integer, Timestamp, Timestamp, Boolean, Integer, Timestamp, Timestamp, Double, String, Long, Long, Short, Short> {

	private static final long serialVersionUID = -1522455363;

	/**
	 * Create a detached TripsRecord
	 */
	public TripsRecord() {
		super(Trips.TRIPS);
	}

	/**
	 * Create a detached, initialised TripsRecord
	 */
	public TripsRecord(Integer id, Integer transportId, Integer driverId, Timestamp departureDate, Timestamp arrivalDate, Boolean isDisabled, Integer routineId, Timestamp changedAt, Timestamp createdAt, Double price, String priceCurrency, Long departurePoint, Long arrivalPoint, Short sitsFree, Short sitsTotal) {
		super(Trips.TRIPS);

		setValue(0, id);
		setValue(1, transportId);
		setValue(2, driverId);
		setValue(3, departureDate);
		setValue(4, arrivalDate);
		setValue(5, isDisabled);
		setValue(6, routineId);
		setValue(7, changedAt);
		setValue(8, createdAt);
		setValue(9, price);
		setValue(10, priceCurrency);
		setValue(11, departurePoint);
		setValue(12, arrivalPoint);
		setValue(13, sitsFree);
		setValue(14, sitsTotal);
	}

	/**
	 * Getter for <code>road_to_b_dev.trips.id</code>.
	 */
	@Id
	@Column(name = "id", unique = true, nullable = false, precision = 10)
	@NotNull
	public Integer getId() {
		return (Integer) getValue(0);
	}

	/**
	 * Setter for <code>road_to_b_dev.trips.id</code>.
	 */
	public TripsRecord setId(Integer value) {
		setValue(0, value);
		return this;
	}

	/**
	 * Getter for <code>road_to_b_dev.trips.transport_id</code>.
	 */
	@Column(name = "transport_id", nullable = false, precision = 10)
	@NotNull
	public Integer getTransportId() {
		return (Integer) getValue(1);
	}

	/**
	 * Setter for <code>road_to_b_dev.trips.transport_id</code>.
	 */
	public TripsRecord setTransportId(Integer value) {
		setValue(1, value);
		return this;
	}

	/**
	 * Getter for <code>road_to_b_dev.trips.driver_id</code>.
	 */
	@Column(name = "driver_id", nullable = false, precision = 10)
	@NotNull
	public Integer getDriverId() {
		return (Integer) getValue(2);
	}

	/**
	 * Setter for <code>road_to_b_dev.trips.driver_id</code>.
	 */
	public TripsRecord setDriverId(Integer value) {
		setValue(2, value);
		return this;
	}

	/**
	 * Getter for <code>road_to_b_dev.trips.departure_date</code>.
	 */
	@Column(name = "departure_date", nullable = false)
	@NotNull
	public Timestamp getDepartureDate() {
		return (Timestamp) getValue(3);
	}

	/**
	 * Setter for <code>road_to_b_dev.trips.departure_date</code>.
	 */
	public TripsRecord setDepartureDate(Timestamp value) {
		setValue(3, value);
		return this;
	}

	/**
	 * Getter for <code>road_to_b_dev.trips.arrival_date</code>.
	 */
	@Column(name = "arrival_date", nullable = false)
	@NotNull
	public Timestamp getArrivalDate() {
		return (Timestamp) getValue(4);
	}

	/**
	 * Setter for <code>road_to_b_dev.trips.arrival_date</code>.
	 */
	public TripsRecord setArrivalDate(Timestamp value) {
		setValue(4, value);
		return this;
	}

	/**
	 * Getter for <code>road_to_b_dev.trips.is_disabled</code>.
	 */
	@Column(name = "is_disabled", nullable = false)
	@NotNull
	public Boolean getIsDisabled() {
		return (Boolean) getValue(5);
	}

	/**
	 * Setter for <code>road_to_b_dev.trips.is_disabled</code>.
	 */
	public TripsRecord setIsDisabled(Boolean value) {
		setValue(5, value);
		return this;
	}

	/**
	 * Getter for <code>road_to_b_dev.trips.routine_id</code>.
	 */
	@Column(name = "routine_id", nullable = false, precision = 10)
	@NotNull
	public Integer getRoutineId() {
		return (Integer) getValue(6);
	}

	/**
	 * Setter for <code>road_to_b_dev.trips.routine_id</code>.
	 */
	public TripsRecord setRoutineId(Integer value) {
		setValue(6, value);
		return this;
	}

	/**
	 * Getter for <code>road_to_b_dev.trips.changed_at</code>.
	 */
	@Column(name = "changed_at", nullable = false)
	@NotNull
	public Timestamp getChangedAt() {
		return (Timestamp) getValue(7);
	}

	/**
	 * Setter for <code>road_to_b_dev.trips.changed_at</code>.
	 */
	public TripsRecord setChangedAt(Timestamp value) {
		setValue(7, value);
		return this;
	}

	/**
	 * Getter for <code>road_to_b_dev.trips.created_at</code>.
	 */
	@Column(name = "created_at", nullable = false)
	@NotNull
	public Timestamp getCreatedAt() {
		return (Timestamp) getValue(8);
	}

	/**
	 * Setter for <code>road_to_b_dev.trips.created_at</code>.
	 */
	public TripsRecord setCreatedAt(Timestamp value) {
		setValue(8, value);
		return this;
	}

	/**
	 * Getter for <code>road_to_b_dev.trips.price</code>.
	 */
	@Column(name = "price", nullable = false, precision = 22)
	@NotNull
	public Double getPrice() {
		return (Double) getValue(9);
	}

	/**
	 * Setter for <code>road_to_b_dev.trips.price</code>.
	 */
	public TripsRecord setPrice(Double value) {
		setValue(9, value);
		return this;
	}

	/**
	 * Getter for <code>road_to_b_dev.trips.price_currency</code>.
	 */
	@Column(name = "price_currency", length = 16)
	@Size(max = 16)
	public String getPriceCurrency() {
		return (String) getValue(10);
	}

	/**
	 * Setter for <code>road_to_b_dev.trips.price_currency</code>.
	 */
	public TripsRecord setPriceCurrency(String value) {
		setValue(10, value);
		return this;
	}

	/**
	 * Getter for <code>road_to_b_dev.trips.departure_point</code>.
	 */
	@Column(name = "departure_point", nullable = false, precision = 19)
	@NotNull
	public Long getDeparturePoint() {
		return (Long) getValue(11);
	}

	/**
	 * Setter for <code>road_to_b_dev.trips.departure_point</code>.
	 */
	public TripsRecord setDeparturePoint(Long value) {
		setValue(11, value);
		return this;
	}

	/**
	 * Getter for <code>road_to_b_dev.trips.arrival_point</code>.
	 */
	@Column(name = "arrival_point", precision = 19)
	public Long getArrivalPoint() {
		return (Long) getValue(12);
	}

	/**
	 * Setter for <code>road_to_b_dev.trips.arrival_point</code>.
	 */
	public TripsRecord setArrivalPoint(Long value) {
		setValue(12, value);
		return this;
	}

	/**
	 * Getter for <code>road_to_b_dev.trips.sits_free</code>.
	 */
	@Column(name = "sits_free", precision = 5)
	public Short getSitsFree() {
		return (Short) getValue(13);
	}

	/**
	 * Setter for <code>road_to_b_dev.trips.sits_free</code>.
	 */
	public TripsRecord setSitsFree(Short value) {
		setValue(13, value);
		return this;
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * Getter for <code>road_to_b_dev.trips.sits_total</code>.
	 */
	@Column(name = "sits_total", precision = 5)
	public Short getSitsTotal() {
		return (Short) getValue(14);
	}

	// -------------------------------------------------------------------------
	// Record15 type implementation
	// -------------------------------------------------------------------------

	/**
	 * Setter for <code>road_to_b_dev.trips.sits_total</code>.
	 */
	public TripsRecord setSitsTotal(Short value) {
		setValue(14, value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record1<Integer> key() {
		return (Record1) super.key();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row15<Integer, Integer, Integer, Timestamp, Timestamp, Boolean, Integer, Timestamp, Timestamp, Double, String, Long, Long, Short, Short> fieldsRow() {
		return (Row15) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row15<Integer, Integer, Integer, Timestamp, Timestamp, Boolean, Integer, Timestamp, Timestamp, Double, String, Long, Long, Short, Short> valuesRow() {
		return (Row15) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Integer> field1() {
		return Trips.TRIPS.ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Integer> field2() {
		return Trips.TRIPS.TRANSPORT_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Integer> field3() {
		return Trips.TRIPS.DRIVER_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Timestamp> field4() {
		return Trips.TRIPS.DEPARTURE_DATE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Timestamp> field5() {
		return Trips.TRIPS.ARRIVAL_DATE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Boolean> field6() {
		return Trips.TRIPS.IS_DISABLED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Integer> field7() {
		return Trips.TRIPS.ROUTINE_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Timestamp> field8() {
		return Trips.TRIPS.CHANGED_AT;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Timestamp> field9() {
		return Trips.TRIPS.CREATED_AT;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Double> field10() {
		return Trips.TRIPS.PRICE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<String> field11() {
		return Trips.TRIPS.PRICE_CURRENCY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field12() {
		return Trips.TRIPS.DEPARTURE_POINT;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field13() {
		return Trips.TRIPS.ARRIVAL_POINT;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Short> field14() {
		return Trips.TRIPS.SITS_FREE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Short> field15() {
		return Trips.TRIPS.SITS_TOTAL;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer value1() {
		return getId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer value2() {
		return getTransportId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer value3() {
		return getDriverId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Timestamp value4() {
		return getDepartureDate();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Timestamp value5() {
		return getArrivalDate();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean value6() {
		return getIsDisabled();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer value7() {
		return getRoutineId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Timestamp value8() {
		return getChangedAt();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Timestamp value9() {
		return getCreatedAt();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double value10() {
		return getPrice();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String value11() {
		return getPriceCurrency();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value12() {
		return getDeparturePoint();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value13() {
		return getArrivalPoint();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Short value14() {
		return getSitsFree();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Short value15() {
		return getSitsTotal();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripsRecord value1(Integer value) {
		setId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripsRecord value2(Integer value) {
		setTransportId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripsRecord value3(Integer value) {
		setDriverId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripsRecord value4(Timestamp value) {
		setDepartureDate(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripsRecord value5(Timestamp value) {
		setArrivalDate(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripsRecord value6(Boolean value) {
		setIsDisabled(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripsRecord value7(Integer value) {
		setRoutineId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripsRecord value8(Timestamp value) {
		setChangedAt(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripsRecord value9(Timestamp value) {
		setCreatedAt(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripsRecord value10(Double value) {
		setPrice(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripsRecord value11(String value) {
		setPriceCurrency(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripsRecord value12(Long value) {
		setDeparturePoint(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripsRecord value13(Long value) {
		setArrivalPoint(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripsRecord value14(Short value) {
		setSitsFree(value);
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripsRecord value15(Short value) {
		setSitsTotal(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TripsRecord values(Integer value1, Integer value2, Integer value3, Timestamp value4, Timestamp value5, Boolean value6, Integer value7, Timestamp value8, Timestamp value9, Double value10, String value11, Long value12, Long value13, Short value14, Short value15) {
		value1(value1);
		value2(value2);
		value3(value3);
		value4(value4);
		value5(value5);
		value6(value6);
		value7(value7);
		value8(value8);
		value9(value9);
		value10(value10);
		value11(value11);
		value12(value12);
		value13(value13);
		value14(value14);
		value15(value15);
		return this;
	}
}
